extends Control


func _ready():
	pass
	
func _on_spellbooks_pressed():
	$spellbooks.hide()
	$cube_container.hide()
	$spellbooks_container.show()
	$cube.show()
	
func _on_cube_pressed():
	$cube.hide()
	$spellbooks_container.hide()
	$cube_container.show()
	$spellbooks.show()
