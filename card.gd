extends Control

func _ready():
	$card_image.global_position = Vector2(0, 0)
	
func _on_label_mouse_entered():
	$card_image.show()
	
func _on_label_mouse_exited():
	$card_image.hide()
